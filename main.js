const express = require("express");
const app = express();
const PORT = 3000;

//build small REST API with Express
console.log("Server-side program starting...");

// baseurl: http://localhost:3000
// Endpoint: http/localhost:3000/
app.get('/', (reg, res) => {
    res.send("Hello world");
})

app.listen(PORT, () =>(
    `Server listening http://localhost:${PORT}`
));